package com.example.petclinic.services;

import com.example.petclinic.entity.Owner;

import java.util.Collection;

public interface OwnerService extends CrudService<Owner, Long> {

    Collection<Owner> findByName(String name);
}
