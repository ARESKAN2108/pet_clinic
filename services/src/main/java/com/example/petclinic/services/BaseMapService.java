package com.example.petclinic.services;

import java.util.Map;

//эти Т, ID никак не пересекаются с CrudService
//это можно сделать в процессе наследования
public interface BaseMapService<T, ID> {

    Map<ID, T> getResource();
}
