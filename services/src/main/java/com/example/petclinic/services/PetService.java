package com.example.petclinic.services;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;

public interface PetService extends CrudService<Pet, Long> {

    Pet findByOwner(Owner owner);
}
