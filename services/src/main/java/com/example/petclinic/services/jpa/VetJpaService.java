package com.example.petclinic.services.jpa;

import com.example.petclinic.entity.Vet;
import com.example.petclinic.services.VetService;
import com.example.petclinic.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

@JpaImplementation
public class VetJpaService extends AbstractJpaService<Vet, Long> implements VetService {

    @Override
    public JpaRepository<Vet, Long> getRepository() {
        //заглушка для нереализованной логики
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Vet> findBySpec(String spec) {
        //заглушка для нереализованной логики
        throw new UnsupportedOperationException();
    }
}
