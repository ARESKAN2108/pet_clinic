package com.example.petclinic.entity;

public interface BaseEntity<ID> {

    ID getId();
}
