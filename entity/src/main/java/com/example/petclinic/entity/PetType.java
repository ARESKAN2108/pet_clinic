package com.example.petclinic.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum PetType {

    CAT("Cat"),
    DOG("Dog");

    private final String value;
}
